"""
This file is loaded as server views by importing app, auth, and models etc
"""
import os
import json
import time
import pytz
import requests
import datetime
import asyncio
from functools import wraps
from multiprocessing import Process, Value
from flask import render_template, request, Response, jsonify
from core.settings.app import app
from core.settings.auth import auth
from .models import User, BiometricUser, Stats
from .fingerprint import execute_sensor
# from .tasks import update_user_db

# @app.route('/post_user/', methods=['GET'])
# def user_posts():
#     return jsonify(update_user_db()), 200

def log():
    """
    Log a person checked in or out
    """
    sensor = execute_sensor()
    if not sensor:
        return jsonify({"error" : "sensor could not be initialized!"}), 400    
    result = sensor.search_record()

    if not result['error']:
        person = BiometricUser.select().where(BiometricUser.bio_id==int(result['pos']))
        if len(person.dicts()) > 0:
            data = person.dicts().get()
            if is_time_between(datetime.time(00,00), datetime.time(4,00)):
                save_model(data['id'], True, sensor)
            else:
                save_model(data['id'], False, sensor)
            sensor.lcd.clear()
            time.sleep(0.2)
            sensor.lcd.set("Name:  %s" % data['name'].split()[0], 1)
            sensor.lcd.set("Id: %s" % data['employee_id'], 2)
            time.sleep(3)
            sensor.lcd.clear()
            return {"error" : False, "result" : data}
        else:
            return {"error" : "User with this id does not exist in DB"}
    return {"error" : True, "result" : result}

def logging(loop_on):
    """
    Keep calling log() when person checked in or out
    """
    while loop_on:
        log()

def is_time_between(begin_time, end_time, check_time=None):
    """
    Returns True if given time is within the specified time range
    """
    check_time = check_time or datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata')).time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else:
        return check_time >= begin_time or check_time <= end_time

def save_model(pk, midnight, sensor):
    """
    if date is in between 12 am to 4 am, find the previous day record(it should be immediate last day)
    and update it instead of creating a record for next day
    """
    log_time = None
    if midnight:
        print("under midnight", datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata')).time())
        log_time = datetime.date.today()-datetime.timedelta(1)
    else:
        print("under daytime", datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata')).time())
        log_time = datetime.date.today()
    data_source = {'user_id' : pk,
                   'created_on' :log_time,
                   'check_in' : datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata')),
                   'is_checked_in' : True}
    record = Stats.select().where(Stats.created_on==log_time,
                                  Stats.user_id==pk).order_by(Stats.id.desc())
    if len(record.dicts()) > 0 and not record.dicts().get()['check_out'] :
        stats = Stats.update(
            check_out=datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata'))).where(
                Stats.created_on==log_time, Stats.user_id==pk, Stats.check_out==None
            ).execute()
        sensor.lcd.clear()
        time.sleep(0.2)
        sensor.lcd.set("   Checked Out !    ", 1)
        time.sleep(1)
    else:
        stats = Stats.create(**data_source)
        stats.save()
        sensor.lcd.clear()
        time.sleep(0.2)
        sensor.lcd.set("    Checked In !    ", 1)
        time.sleep(1)
        
def save_model_old(pk):
    """
    Method to record when user logs
    """
    record = Stats.select().where(Stats.created_on==datetime.date.today(),
                                  Stats.user_id==pk).order_by(Stats.id.desc())
    if len(record.dicts()) > 0 and not record.dicts().get()['check_out'] :
        stats = Stats.update(
            check_out=datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata'))).where(
                Stats.created_on==datetime.date.today(), Stats.user_id==pk, Stats.check_out==None
            ).execute()
    else:
        stats = Stats.create(user_id=pk,
                             check_in=datetime.datetime.now(tz=pytz.timezone('Asia/Kolkata')),
                             is_checked_in=True)
        stats.save()

def admin_login_required(f):
    """
    Decorater to check whether user is admin or not
    """
    @wraps(f)
    def wrap(*args, **kwargs):
        token = request.headers.get("Authorization")
        if token is None:
            return {"error" : "token required"}
        user = User.verify_auth_token(token)
        if not user:
            return {"error" : "User authentication failed"}
        if user['error'] is None:
            if not user['admin']:
                return {"error" : "you need to be an admin"}
        if user['error']:
            return user
        return f(*args, **kwargs)
    return wrap

@app.route('/api/login/', methods=['POST'])
def login():
    """
    Method that returns token by passing username and password
    """
    data = request.get_json(force=True, silent=True)
    if not data.get('username') and not data.get('password'):
        return jsonify({"error" : "username or password not found"}), 400
    user =  auth.authenticate(data["username"] , data["password"])
    if user:
        token = user.generate_auth_token()
        return jsonify({ 'token': token.decode('ascii') }), 200
    return jsonify({"error" : "Invalid user"}), 404

@app.route('/')
def homepage():
    """
    Method for default page
    """
    return render_template('base2.html')

@app.route('/change-mode/', methods=['GET'])
def user_mode():
    """
    Change admin/user mode 
    """
    try:
        print("thread while user mode on/off ==> ", app.config['thread'].is_alive())
    except Exception as e:
        print(e)
    if app.config['thread'] is not None and app.config['thread'].is_alive():
        #print('under if', app.config['thread'])
        app.config['thread'].terminate()
        time.sleep(2)
        return jsonify("Admin mode start"), 200
    elif app.config['thread'] is None or not app.config['thread'].is_alive():
        #print('under else', app.config['thread'])
        p = Process(target=logging, args=(True,))
        p.start()
        app.config['thread'] = p
        return jsonify("User mode start"), 200
    return jsonify("nothing has happend"), 200

@app.route('/biometric-user/register/', methods=['POST'])
@admin_login_required
def enroll_user():
    """
    Enroll User to biometric sensor's memory and Sqlite3 DB
    """
    if app.config['thread'] and app.config['thread'].is_alive():
        app.config['thread'].terminate()
        time.sleep(2)
    def should_enroll_user():
        data = request.get_json(force=True, silent=True)
        if not (data.get('name') and data.get('employee_id')):
            return jsonify({"error" : "Name or Employee Id not found"}), 400
        sensor = execute_sensor()
        if not sensor:
            return jsonify({"error" : "sensor could not be initialized!"}), 400
        result = sensor.enroll()
        if not result['error']:
            bio = BiometricUser.create(name=data['name'],
                                       employee_id=data['employee_id'],
                                       bio_id=int(result['pos']),
                                       credentials=result['cred_hash'])
            bio.save()
            sensor.lcd.clear()
            time.sleep(1)
            return jsonify(result), 201
        sensor.lcd.clear()
        time.sleep(1)
        return jsonify(result), 404
    obj = should_enroll_user()
    return obj

@app.route('/biometric-user/<int:user_id>/', methods=['DELETE'])
@admin_login_required
def delete_user(user_id):
    """
    Method to delete user from Biometric sensor as well as DB
    """
    if app.config['thread'] and app.config['thread'].is_alive():
        app.config['thread'].terminate()
        time.sleep(2)
    sensor = execute_sensor()
    if app.config['thread'].is_alive():
        app.config['thread'].terminate()
        time.sleep(1)
    if not sensor:
        return jsonify({"error" : "sensor could not be initialized!"}), 400
    result = BiometricUser.select().where(BiometricUser.id==user_id)
    print("result => ", result.dicts())
    if len(result.dicts()) > 0:
        data = sensor.delete_record(result.dicts()[0]['bio_id'])
        if not data['error']:
            BiometricUser.delete().where(BiometricUser.id==user_id).execute()
            return jsonify("User deleted"), 200
        return jsonify(data), 400
    return jsonify({"error" : "unable to delete user"}), 404
