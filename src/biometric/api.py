"""
api imports app, auth and models
"""
from flask_peewee.rest import RestAPI, RestResource, UserAuthentication
from core.settings.app import app
from core.settings.auth import auth
from .models import User, Stats

user_auth = UserAuthentication(auth)
api = RestAPI(app, default_auth=user_auth)

class UserResource(RestResource):
    """
    Instantiate our api wrapper and tell it to use HTTP basic auth using
    the same credentials as our auth system. If you prefer this could instead
    be a key-based auth, or good to forbid some open auth protocol.
    """
    exclude = ('password',)

# class StatsResource(RestResource):
#     #exclude = ('password', 'email',)
#     paginate_by = 5

# register our models so they are exposed via /api/<model>/
api.register(User, UserResource, auth=user_auth)
#api.register(User, StatsResource)
