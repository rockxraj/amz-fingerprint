import requests
import json
from .models import Stats, BiometricUser
from core.settings.config import Configuration

def create_user_statistics_list():
    """
    Returns User statistics by generating records on the basis of created_on  
    """
    print('\033[94m' + "Creating Stats backup...." + '\033[0m')
    list_data = []
    data = {}
    query = Stats.select(Stats.user_id).distinct()
    for user in query:
        for date in Stats.select(Stats.created_on).distinct():
            result = generate_records(user, date.created_on)
            if result:
                list_data.append(result)
    return list_data

def generate_biometric_user_list():
    """
    returns the total number of biometric users
    """
    print('\033[94m' + "Creating Biometric User backup...." + '\033[0m')
    data = []
    query = BiometricUser.select()
    for obj in query:
        data.append({
            "remote_user_id" : obj.id,
            "name" : obj.name,
            "bio_id" : obj.bio_id,
            "credentials" : obj.credentials,
            "employee_id" : obj.employee_id,
            "created_on" : obj.created_on,
            "updated_on" : obj.updated_on
        })
    return data

def generate_records(user, date):
    """
    Generates records per user and date wise
    """
    query = Stats.select().where(Stats.user_id == user.user_id, Stats.created_on == date)
    same = False
    data = {}
    for obj in query:
        if not same:
            same = True
            data = {
                'biometric_user' : obj.user_id.id,
                'employee_id' : obj.user_id.employee_id,
                'attendance_created_on' : default_date(obj.created_on),
                'attendance_log_details' : [{'check_in' : obj.check_in,
                                             'check_out' : obj.check_out}]
            }
        else:
            data['attendance_log_details'].append({'check_in' : obj.check_in,
                                                   'check_out' : obj.check_out})
    return data

def post_data(list_data, url):
    """
    Method to post daily data to given url
    """
    response = requests.post(url, data=json.dumps(list_data), headers = {'content-type': "application/json",})
    print("response.ok => ", response.json())
    return response

def default_date(obj):
    """
    Converts date object to string type
    """
    if obj:
        return obj.strftime("%Y-%m-%d %H:%M:%S")

def flush_table_records(table):
    """
    Delete all records from table and return number of deleted rows
    """
    query = table.delete().execute()
    return query

def update_stats_db():
    """
    Updates the remote server's Database
    """
    list_data = create_user_statistics_list()
    response = post_data(list_data, Configuration.STATS_URL)
    if response.ok:
        print("\033[92m" + "Back created successfully!!!" + "\033[0m")
        print("\033[93m" + "Flushing Stats records from Database ... " + "\033[0m")
        result = flush_table_records(Stats)
        if result:
            print("\033[92m" + str(result) +" records deleted successfully!!" + "\033[0m")

def update_user_db():
    """
    Update user records
    """
    list_data = generate_biometric_user_list()
    response = post_data(list_data, Configuration.BIOMETRIC_USER_URL)
    print("user res ==> ", response)
    if response.ok:
        print("posted..")

def update_db():
    """
    Update both databases
    """
    update_user_db()
    update_stats_db()
