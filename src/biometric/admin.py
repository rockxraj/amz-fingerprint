"""
admin imports app, auth and models
"""
from flask_peewee.admin import Admin, ModelAdmin, AdminPanel
from core.settings.app import app, db
from core.settings.auth import auth
from .models import User, BiometricUser, Stats

admin = Admin(app, auth)
auth.register_admin(admin)

class UserAdmin(ModelAdmin):
    """
    Customize User's admin panel
    """
    columns = ('id', 'username', 'email', 'join_date', 'active', 'admin')
    filter_fields = ('username', 'email', 'admin')
    fields = ('id', 'username', 'password', 'email', 'active', 'admin')
    
class BiometricAdmin(ModelAdmin):
    """
    Customize Biometric User's admin panel
    """
    columns = ('id', 'name', 'bio_id', 'employee_id', 'created_on', 'updated_on')
    filter_fields = ('name', 'bio_id', 'created_on')
    fields = ('name')

class StatsAdmin(ModelAdmin):
    """
    Customize  biometric user records admin panel
    """
    columns = ('id', 'user_id', 'created_on', 'check_in', 'check_out')
    filter_fields = ('user_id', 'created_on', 'employee_id')
    exclude = ('user_id', 'created_on', 'check_in', 'check_out')
        
admin.register(User, UserAdmin)
admin.register(Stats, StatsAdmin)
admin.register(BiometricUser, BiometricAdmin)
