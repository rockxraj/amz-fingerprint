"""
models imports app, but app does not import models so we haven't created
any loops.
"""
import datetime
import pytz
from flask_peewee.auth import BaseUser  # provides password helpers
from peewee import *
from core.settings.app import db
from core.settings.config import Configuration
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

class User(db.Model, BaseUser):
    """
    Models for system users
    """
    username = CharField(unique=True)
    password = CharField()
    email = CharField()
    join_date = DateTimeField(default=datetime.datetime.now(
        tz=pytz.timezone('Asia/Kolkata')))
    active = BooleanField(default=True)
    admin = BooleanField(default=False)

    def __unicode__(self):
        """
        Represents object on the basis of username
        """
        return self.username

    def generate_auth_token(self, expiration = 6000):
        """
        Generate token that expires in 1 hour
        """
        s = Serializer(Configuration.SECRET_KEY, expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        """
        Method to verify the token
        """
        s = Serializer(Configuration.SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired as e:
            return {"error" : "Token Expired"} # valid token, but expired
        except BadSignature as e:
            return {"error" : "Invalid token" } # invalid token
        user = User.get(data['id'])
        return {"error" : None, "admin" : user.admin, "active" : user.active}
    
class BiometricUser(db.Model):
    """
    Models for creating biometric users
    """
    name = CharField(max_length=255, index=True)
    employee_id = CharField(max_length=255, index=True)
    bio_id = IntegerField(index=True)
    credentials = TextField(index=True)
    created_on = DateTimeField(default=datetime.datetime.now(
        tz=pytz.timezone('Asia/Kolkata')))
    updated_on = DateTimeField(default=datetime.datetime.now(
        tz=pytz.timezone('Asia/Kolkata')))

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        """
        Saves the updated time, primarily used from admin panel
        """
        self.updated_on = datetime.datetime.now(
        tz=pytz.timezone('Asia/Kolkata'))
        return super(BiometricUser, self).save(*args, **kwargs)
    
class Stats(db.Model):
    """
    Models to store biometric records
    """
    user_id = ForeignKeyField(BiometricUser)
    created_on = DateField(default=datetime.date.today())
    check_in = DateTimeField(null=True)
    check_out = DateTimeField(null=True)

    def __str__(self):
        return self.user_id.name
    
    # def statistics(self):
    #     """
    #     Expose the stats through API
    #     """
    #     list_data = []
    #     query = Stats.select().where(Stats.user_id==self)
    #     for obj in query:
    #         list_data.append({
    #             "id" : obj.user_id,
    #             "name" : obj.user_id.name,
    #             "bio_id" : obj.user_id.bio_id,
    #             "credentials" : obj.user_id.credentials,
    #             "created_on" : obj.user_id.created_on,
    #             "updated_on" : obj.user_id.updated_on,
    #             "attendance_created_on" : obj.created_on,
    #             "check_in" : obj.check_in,
    #             "check_out" : obj.check_out})
    #     return list_data
