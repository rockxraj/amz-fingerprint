"""
This file is intended to work with LCD Screen
"""
import time
from RPi_GPIO_i2c_LCD import lcd

class Sender:
    """
    Initialize the display to send messages in I2C LCD Screen
    """
    
    def __init__(self):
        """
        Initailize lcd drivers
        """
        self.lcd_display = lcd.HD44780(0x27)

    def set(self, message, line):
        """
        Set the message by passing content and line number
        """
        self.lcd_display.set(message, line)
        
    def clear(self):
        """
        Clears the screen 
        """
        #self.lcd_display.lcd.clear()
        self.set(" " * 20, 1)
        self.set(" " * 20, 2)
        self.set(" " * 20, 3)
        self.set(" " * 20, 4)
        time.sleep(0.2)
