"""
This file written to work with R-307 sensor that will take input 
from fingerprint sensor and produce output by its inbuit algorithm 
"""
import time
import sys
import hashlib
from pyfingerprint.pyfingerprint import PyFingerprint
from .display import Sender

class Fingerprint:
    """
    Class to intialize biometric sensor along with LCD display
    """
    @classmethod
    def _get_sensor(cls):
        """
        Method to create Sensor intance once at the begning 
        """
        #print("_get_sensor => ", hasattr(cls, '_sensor'))
        if not hasattr(cls, '_sensor'):
            cls._sensor = PyFingerprint('/dev/ttyAMA0', 57600, 0xFFFFFFFF, 0x00000000)
            # cls._sensor = PyFingerprint('/dev/ttyUSB0', 57600, 0xFFFFFFFF, 0x00000000)
            cls._lcd = Sender() 
        return cls._sensor, cls._lcd
    
    def __init__(self):
        """
        Initialize object with sensors and LCD
        """
        try:
            self.sensor, self.lcd = self._get_sensor()
            if ( self.sensor.verifyPassword() == False ):
                return {'error' : 'The given fingerprint sensor password is wrong!'}
        
        except Exception as e:
            print('The fingerprint sensor could not be initialized!', e)
            return None

    def search_record(self):
        """
        Search records using R-307 algorithm with less than 0.5 second
        """
        try:
            print('Waiting for finger...')
            self.lcd.set("Waiting for input..", 1)
            while ( self.sensor.readImage() == False ):
                pass
            self.sensor.convertImage(0x01)
            result = self.sensor.searchTemplate()
            positionNumber = result[0]
            accuracyScore = result[1]
            self.lcd.clear()
            time.sleep(0.2)
            if ( positionNumber == -1 ):
                self.lcd.set("No match found!!!",1)
                time.sleep(3)
                self.lcd.clear()
                time.sleep(0.2)
                return {"error" : "No match found!!!!!"}
            else:
                print('Found template at position #' + str(positionNumber))
                self.sensor.loadTemplate(positionNumber, 0x01)
                characterics = str(self.sensor.downloadCharacteristics(0x01)).encode('utf-8')
                # print('SHA-2 hash of template: ' + hashlib.sha256(characterics).hexdigest())
                print(str(accuracyScore))
                if int(str(accuracyScore)) >= 58:
                    return {"error" : None, "pos" : str(positionNumber), "accuracy_score" : str(accuracyScore)}
                self.lcd.set("Clean your finger", 1)
                self.lcd.set("and Retry", 2)
                time.sleep(3)
                return {"error" : "Clean your finger to identify you better"}
        except Exception as e:
            print('Search Operation failed!', e)
            self.lcd.set('Operation failed !!!', 1)
            time.sleep(2)
            self.lcd.clear()
            time.sleep(1)
            return {"error" : True}

    def enroll(self):
        """
        Method to enroll Biometric Users upto 1000 records and return Position of the template
        """
        try:
            print('Waiting for finger...')
            self.lcd.set("*******Enroll*******",1)
            self.lcd.set("Waiting for finger..",2)
            while ( self.sensor.readImage() == False ):
                pass
            self.sensor.convertImage(0x01)
            result = self.sensor.searchTemplate()
            positionNumber = result[0]
            if ( positionNumber >= 0 ):
                print('Template already exists at position #' + str(positionNumber))
                return {"error" : "Template already exists at position #" + str(positionNumber)}
            print('Remove finger...')
            self.lcd.clear()
            time.sleep(0.2)
            self.lcd.set("Remove finger ......",1)
            time.sleep(2)
            print('Waiting for same finger again...')
            self.lcd.set("Waiting for the same ",1)
            self.lcd.set("finger again .......", 2)
            while ( self.sensor.readImage() == False ):
                pass
            self.sensor.convertImage(0x02)
            self.lcd.clear()
            time.sleep(0.2)
            if ( self.sensor.compareCharacteristics() == 0 ):
                self.lcd.set("Fingers didn't match !", 1)
                time.sleep(1)
                self.lcd.clear()
                return {"error" : "Fingers didn't match"}
            self.sensor.createTemplate()
            positionNumber = self.sensor.storeTemplate()
            print('Finger enrolled successfully!')
            self.lcd.set("Finger enrolled", 1)
            self.lcd.set("successfully!!!", 2)
            time.sleep(3)
            print('New template position #' + str(positionNumber))
            self.sensor.loadTemplate(positionNumber, 0x01)
            characterics = str(self.sensor.downloadCharacteristics(0x01)).encode('utf-8')
            credential_hash = hashlib.sha256(characterics).hexdigest()
            return {"error" : None, "pos" :str(positionNumber), "cred_hash" : credential_hash }
        except Exception as e:
            print('Operation failed.. Exception message: ' + str(e))
            self.lcd.clear()
            time.sleep(1)
            self.lcd.set("Operation Failed!", 1)
            time.sleep(2)
            return {"error" : 'Operation failed !!!'}
        
    def delete_record(self, position_number):
        """
        Method takes position number of Biometric User
        """
        print('Currently used templates: ' + str(self.sensor.getTemplateCount()) +'/'+ str(self.sensor.getStorageCapacity()))
        try:
            if ( self.sensor.deleteTemplate(position_number) == True ):
                print('Template deleted!')
                self.lcd.clear()
                time.sleep(0.5)
                self.lcd.set("User deleted!", 1)
                time.sleep(2)
                self.lcd.clear()
                time.sleep(0.5)
                return {"error" : None}
        except Exception as e:
            print('Exception message: ' + str(e))
            self.lcd.clear()
            time.sleep(0.5)
            self.lcd.set("Operation Failed!", 1)
            time.sleep(2)
            return {"error": str(e)}
        
def execute_sensor():
    """
    Method to create fingerprint class's object if successfully created otherwise None
    """
    finger_obj = Fingerprint()
    if hasattr(finger_obj, 'lcd') and hasattr(finger_obj, 'sensor'):
        return finger_obj
    return None
