"""
keep app.py very thin.
"""
import os
from flask import Flask
from flask_peewee.db import Database

app = Flask(__name__)
app.config.from_object('core.settings.config.Configuration')
db = Database(app)
