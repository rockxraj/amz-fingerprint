"""
Configurations for the Flask server
"""
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

class Configuration(object):
    DATABASE = {
        'name': os.path.join(BASE_DIR, 'stats.db'),
        'engine': 'peewee.SqliteDatabase',
        'check_same_thread': False,
    }
    DEBUG = True
    SECRET_KEY = '\x8b\x1d\xd1\xda\xc0y\xfc\xb7\x7f/\xaaslR\x17\x8c'
    api_url = "https://amz-fingerprint.herokuapp.com"
    #api_url = "http://127.0.0.1:8000"
    STATS_URL = api_url + "/biometric-analytics/"
    BIOMETRIC_USER_URL = api_url + "/biometric-user/"
