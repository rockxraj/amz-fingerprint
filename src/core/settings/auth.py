"""
auth imports app and models
"""
from flask_peewee.auth import Auth  # Login/logout views, etc.
from .app import app, db
from biometric.models import User

auth = Auth(app, db, user_model=User)
