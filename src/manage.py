import os
import atexit
import getpass
from multiprocessing import Process
from flask_script import Manager
from core.settings.app import app, db
from biometric.models import User, BiometricUser, Stats
from biometric.api import api
from biometric.admin import admin
from biometric.views import logging
from biometric.tasks import update_db
from apscheduler.triggers.cron import CronTrigger
from apscheduler.schedulers.background import BackgroundScheduler

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    
class bcolors:
    """Text colors to prints text on terminal."""
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

manager = Manager(app)

def create_tables():
    """Create tables by giving models."""
    db.database.create_tables([User, BiometricUser, Stats], safe=True)

@manager.command
def createsuperuser():
    """Create's the Admin."""
    while True:
        username = input("Username: ")
        user = User.get_or_none(User.username==username)
        if user:
            print(bcolors.FAIL + "Error: That username is already taken." + bcolors.ENDC)
            continue
        if not username:
            print(bcolors.FAIL + "Error: This field cannot be blank." + bcolors.ENDC)
            continue
        break
    email = input("Email: ")
    while True:
        password = getpass.getpass("Password: ")
        if not password:
            print(bcolors.FAIL + "Error: This field cannot be blank." + bcolors.ENDC)
            continue
        break
    while True:
        password_again = getpass.getpass("Password (again): ")
        if not password_again:
            print(bcolors.FAIL + "Error: This field cannot be blank." + bcolors.ENDC)
            continue
        if password != password_again:
            print(bcolors.FAIL + "Error: Your passwords didn't match." + bcolors.ENDC)
            continue
        break
    admin = User(username=username, email=email, admin=True, active=True)
    admin.set_password(password)
    admin.save()
    print(bcolors.OKGREEN + "Superuser created successfully." + bcolors.ENDC)
    
@manager.command
def create_db():
    """Creates the db tables."""
    create_tables()

@manager.command
def runserver():
    """ Run's the server"""
    cron = BackgroundScheduler()
    trigger = CronTrigger(hour=7, minute=30, timezone="Asia/Kolkata")
    cron.add_job(func=update_db, trigger=trigger)
    cron.start()
    atexit.register(lambda: cron.shutdown())
    thread = Process(target=logging, args=(True, ))
    thread.start()
    app.config['thread'] = thread
    app.run(host = '0.0.0.0', use_reloader=False)
    
if __name__ == '__main__':
    admin.setup()
    api.setup()
    manager.run()
